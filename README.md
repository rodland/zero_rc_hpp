# ZERO_RC_HPP

ZERO_RC_HPP is a fork of the library which seeks a solution of a scalar nonlinear equation f(x)=0, using reverse communication (RC), originally created by Richard Brent and and John Burkardt (https://people.sc.fsu.edu/~jburkardt/cpp_src/zero_rc/zero_rc.html).

This particular version is presented as a header-only library for C++ and CUDA.